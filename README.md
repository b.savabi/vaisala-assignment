
# Vaisala assignment

## Toolchain (modified react-create-app)

- TypeScript
- React v17.0.2
- styled-components
- eslint
- testing-library (Jest, Cypress)
- React Mapbox (react-map-gl)

## Key Features

- Requested features are delivered
- Accessibility consideration
- Components and views have proper test coverage 
- End to end test using Cypress
- Tokenized style implementation using styled-components  

## Available Scripts

In the project directory, you can run:

### `REACT_APP_MAPBOX_ACCESS_TOKEN={YOUR_TOKEN} npm start`
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run test`
Launches the test runner in the interactive watch mode.

### `npm run lint`
To run lint tests.

### `npm run build`
Builds the app for production to the `build` folder.

### `npm run e2e:open`
Open Cypress dashboard

### `npm run e2e:run`
Run Cypress tests
