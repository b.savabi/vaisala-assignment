// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add(
  'dropFile',
  {
    prevSubject: false
  },
  (fileName, el) => {
    Cypress.log({
      name: 'dropFile'
    });
    return cy
      .fixture(fileName, 'utf-8')
      .then((blob) => {
        cy.window().then((win) => {
          const file = new win.File(
            [JSON.stringify(blob)],
            fileName,
            { type: 'application/json' }
          );
          const dataTransfer = new win.DataTransfer();
          dataTransfer.items.add(file);
          cy.get('[data-test-key="app-layout"]').trigger('drop', {
            dataTransfer
          });
        });
      });
  }
);

declare namespace Cypress {
  interface Chainable<Subject> {
    dropFile(name: string): Chainable<Subject>;
  }
}