/// <reference types="cypress" />
import mapTempToColor from '../../src/utils/map-temp-to-color';

const VALID_DATA_FILE = 'valid-data.json';
const INVALID_DATA_FILE = 'invalid-data.json';

const cConvert = (value) => Math.round(value * 100) / 100;
const fConvert = (value) => Math.round(((value * 1.8) + 32) * 100) / 100;

context('Functional test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/')
  });

  it('Initial empty state render', () => {
    cy.get('[data-test-key="app-layout"]').should('have.length', 1);
    cy.get('[data-test-key="unit-button"]').should('have.length', 1);
    cy.get('[data-test-key="app-banner"]').should('have.length', 1);
    cy.get('[data-test-key="empty"]').should('have.length', 1);
    cy.get('div.mapboxgl-map').should('have.length', 2);
  });

  it('Render cities', () => {
    cy.dropFile(VALID_DATA_FILE);
    cy.get('[data-test-key="marker-content"]').should('have.length', 3);
    cy.get('[data-test-key="toast"]').should('have.length', 1);
  });

  it('Render error state', () => {
    cy.dropFile(INVALID_DATA_FILE);
    cy.get('[data-test-key="error"]').should('have.length', 1);
  });

  it('Update error state with markers', () => {
    cy.dropFile(INVALID_DATA_FILE);
    cy.get('[data-test-key="error"]').should('have.length', 1);
    cy.dropFile(VALID_DATA_FILE);
    cy.get('[data-test-key="marker-content"]').should('have.length', 3);
    cy.get('[data-test-key="toast"]').should('have.length', 1);
  });

  it('Render correct markers name', () => {
    cy.fixture(VALID_DATA_FILE, 'utf-8')
      .then((dataArray) => {
        dataArray.map(item => {
          cy.dropFile(VALID_DATA_FILE);
          cy.get('[data-test-key="marker-content"]').should('have.length', dataArray.length);
          cy.contains(item.city);
        });      
      });  
  });

  it('Render correct markers value - celsius', () => {
    cy.fixture(VALID_DATA_FILE, 'utf-8')
      .then((dataArray) => {
        dataArray.map(item => {
          cy.dropFile(VALID_DATA_FILE);
          cy.get('[data-test-key="marker-content"]').should('have.length', dataArray.length);
          cy.contains(cConvert(item.temp));
        });      
      });  
  });

  it('Render correct markers value - fahrenheit', () => {
    cy.fixture(VALID_DATA_FILE, 'utf-8')
      .then((dataArray) => {
        cy.get('[data-test-key="unit-button"]').click();
        dataArray.map(item => {
          cy.dropFile(VALID_DATA_FILE);
          cy.get('[data-test-key="marker-content"]').should('have.length', dataArray.length);
          cy.contains(fConvert(item.temp));
        });      
      });  
  });
});

context('Visual test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/')
  });

  it('Render correct markers color', () => {
    cy.fixture(VALID_DATA_FILE, 'utf-8')
      .then((dataArray) => {
        cy.get('[data-test-key="unit-button"]').click();
        dataArray.map((item, index) => {
          cy.dropFile(VALID_DATA_FILE);
          cy.get('[data-test-key="marker-content"]').should('have.length', dataArray.length);
          cy.get('[data-test-key="marker-content"]')
            .eq(index).find('dl').should('have.css', 'background-color', mapTempToColor(item.temp));
          cy.contains(fConvert(item.temp));
        });      
      });  
  });

});