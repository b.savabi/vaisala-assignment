const Typography = {
  city: {
    fontFamily: 'IBM Plex Sans',
    fontWeight: 700,
    fontStyle: 'normal',
    fontSize: '15px',
    textAlign: 'center',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  temperature: {
    fontFamily: 'IBM Plex Sans',
    fontWeight: 400,
    fontStyle: 'normal',
    fontSize: '36px',
    textAlign: 'center',
  },
  h1: {
    fontFamily: 'IBM Plex Sans',
    fontSize: '35px',
    fontWeight: 700,
    letterSpacing: '-0.4px',
    lineHeight: '100px',
  },
  h2: {
    fontFamily: 'IBM Plex Sans',
    fontSize: '16px',
    fontWeight: 700,
    letterSpacing: '0',
    lineHeight: '24px',
  },
  description: {
    fontFamily: 'IBM Plex Sans',
    fontWeight: 400,
    fontSize: '16px',
    letterSpacing: 0,
    lineHeight: '24px',
  },
  interactive: {
    fontFamily: 'IBM Plex Sans',
    fontWeight: 700,
    fontSize: '16px',
    letterSpacing: 0,
    lineHeight: '24px',
  },
  emptyState: {
    fontFamily: 'IBM Plex Sans',
    fontWeight: 700,
    fontSize: '20px',
    letterSpacing: 0,
    lineHeight: '24px',
  },
};

export default Typography;
