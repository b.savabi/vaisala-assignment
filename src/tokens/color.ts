const Color = {
  brightContent: '#efefef',
  lightContainer: '#29292d',
  primaryColor: '#285657',
  interactiveContainer: '#222222',
  focusedBorder: '#00acae',
  descriptionTitle: '#5f5f5f',
  emptyState: 'rgba(0, 0, 0, 0.7)',

  hot: 'rgb(237, 85, 59)',
  moderate: 'rgb(48, 213, 137)',
  cold: 'rgb(33, 174, 255)',
};

export default Color;
