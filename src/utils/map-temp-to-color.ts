import Color from '../tokens/color';

export default (temperature: number): string => {
  if (temperature > 15) {
    return Color.hot;
  }
  if (temperature > 0) {
    return Color.moderate;
  }
  return Color.cold;
};
