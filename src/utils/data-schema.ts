import { Validator } from 'jsonschema';

Validator.prototype.customFormats.latFormat = (input) => !Number.isNaN(parseFloat(input))
  && (parseFloat(input) > -90 && parseFloat(input) < 90);

Validator.prototype.customFormats.lonFormat = (input) => !Number.isNaN(parseFloat(input))
  && (parseFloat(input) >= -180 && parseFloat(input) <= 180);

Validator.prototype.customFormats.tempFormat = (input) => !Number.isNaN(parseFloat(input));

export default {
  type: 'array',
  items: {
    properties: {
      city: { type: 'string' },
      lat: { type: 'string', format: 'latFormat' },
      lon: { type: 'string', format: 'lonFormat' },
      temp: { type: 'string', format: 'tempFormat' },
    },
    required: ['city', 'lat', 'lon', 'temp'],
  },
};
