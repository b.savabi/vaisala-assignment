export default {
  c: {
    label: '°C',
    convert: (value: number):number => Math.round(value * 100) / 100,
    key: 'c',
  },
  f: {
    label: '°F',
    convert: (value: number):number => Math.round(((value * 1.8) + 32) * 100) / 100,
    key: 'f',
  },
};
