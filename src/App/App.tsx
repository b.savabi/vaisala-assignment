import React, { useState } from 'react';
import { validate } from 'jsonschema';
import AppBanner from '../components/AppBanner';
import AppLayout from '../components/AppLayout';
import Icon from '../components/Icon';
import EmptyState from '../components/EmptyState';
import ToastMessage from '../components/ToastMessage';
import Map from '../components/Map';
import Logo from '../assets/logo.png';
import Button from '../components/Button';

import Units from '../utils/units';
import getKey from '../utils/get-key';
import DATA_SCHEMA from '../utils/data-schema';

import AppContext from './AppContext';

const UPDATE_MSG = 'Drag a JSON file anywhere to update the temperatures!';
const EMPTY_MSG = 'Drag a JSON file anywhere to render the temperatures!';
const ERROR_MSG = 'Parsing JSON file is failed, please check the file and try again.';
const LOADING_MSG = 'Parsing JSON file.';

const LOGO_CONFIG = {
  'data-test-key': 'app-render',
  alt: 'Vaisala City Temperature',
  src: Logo,
  style: {
    width: 222,
    height: 46,
  },
};

const stopEvent = (event) => {
  event.preventDefault();
  event.stopPropagation();
};

const App: React.FC = () => {
  const [cities, setCities] = useState(undefined);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [unit, setUnit] = useState(Units.c.key);

  const handleButtonClick = () => {
    setUnit(unit === Units.c.key
      ? Units.f.key
      : Units.c.key);
  };

  const startLoading = () => {
    setLoading(true);
    setError(false);
    setCities(undefined);
  };

  const loadingFailed = () => {
    setLoading(false);
    setError(true);
  };

  const loadingSuccess = (parsedCities) => {
    setLoading(false);
    setCities(parsedCities);
  };

  const handleOnDragOver = (event) => {
    stopEvent(event);
  };

  const validateCities = (parsedJSON) => validate(parsedJSON, DATA_SCHEMA);

  const handleOnDrop = (event) => {
    stopEvent(event);
    startLoading();
    const file = event.dataTransfer.files[0];
    if (file.type.match('application/json')) {
      const reader = new FileReader();
      reader.onload = ({ target: { result } }) => {
        try {
          const parsedCities = JSON.parse(JSON.parse(JSON.stringify(result)));
          const validationResult = validateCities(parsedCities);
          if (validationResult.valid) {
            loadingSuccess(parsedCities);
          } else {
            loadingFailed();
          }
        } catch (errorM) {
          loadingFailed();
        }
      };
      reader.readAsText(file);
    } else {
      setLoading(false);
      setError(true);
    }
  };

  const renderStatus = () => {
    if (loading) {
      return <EmptyState {...getKey('loading')}>{LOADING_MSG}</EmptyState>;
    }
    if (error) {
      return <EmptyState {...getKey('error')}>{ERROR_MSG}</EmptyState>;
    }
    if (!cities) {
      return <EmptyState {...getKey('empty')}>{EMPTY_MSG}</EmptyState>;
    }
    return null;
  };

  const renderToast = () => cities && !loading && !error
    ? <ToastMessage {...getKey('toast')}>{UPDATE_MSG}</ToastMessage>
    : null;

  return (
    <AppContext.Provider
      value={Units[unit].convert}
    >
      <AppLayout
        onDragOver={handleOnDragOver}
        onDrop={handleOnDrop}
        {...getKey('app-layout')}
      >
        <AppBanner {...getKey('app-banner')}>
          <Icon {...LOGO_CONFIG} />
          <Button
            onClick={handleButtonClick}
            {...getKey('unit-button')}
          >
            {Units[unit].label}
          </Button>
        </AppBanner>
        {renderStatus()}
        <Map cities={cities} />
        {renderToast()}
      </AppLayout>
    </AppContext.Provider>
  );
};

export default App;
