import React from 'react';
import { render } from '@testing-library/react';
import  App from './App';

test('render loaded state', () => {
  const { container } = render(< App />);
  expect(container).toMatchSnapshot();
});

