import React from 'react';
import Styled from './styled';

import mapTempToColor from '../../utils/map-temp-to-color';

import AppContext from '../../App/AppContext';

interface Props {
  city: string;
  temperature: number;
  flip: boolean;
}

const height = 120;
const width = 120;

/**
 * `MarkerContent` can be used to render markers on the map showing city name and temperature.
 * All props will be passed to the root element.
 */
export const MarkerContent = React.forwardRef<HTMLDivElement, Props>(
  ({
    flip,
    city,
    temperature,
    ...otherProps
  }, ref) => {
    const convert = React.useContext(AppContext);
    const backgroundColor = mapTempToColor(temperature);
    return (
      <Styled.MarkerContent
        ref={ref}
        {...otherProps}
      >
        {flip && <Styled.ArrowTop $backgroundColor={backgroundColor} />}
        <Styled.MarkerWrapper
          style={{
            height: height - 25,
            width,
            backgroundColor,
          }}
        >
          <Styled.MarkerCity>
            {city}
          </Styled.MarkerCity>
          <Styled.MarkerTemperature>
            {convert(temperature)}
          </Styled.MarkerTemperature>
        </Styled.MarkerWrapper>
        {!flip && <Styled.ArrowDown $backgroundColor={backgroundColor} />}
      </Styled.MarkerContent>
    );
  },
);

export {
  height,
  width,
};

export default MarkerContent;
