import React from 'react';
import { render } from '@testing-library/react';
import AppContext from '../../App/AppContext';
import Units from '../../utils/units';

import MarkerContent from '.';

const DEFAULT_CONFIG = {
  city: 'City',
  temperature: 13,
};

test('render', () => {
  const { container } = render(
    <AppContext.Provider value={Units['c'].convert}>
      <MarkerContent {...DEFAULT_CONFIG} flip={false} />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});

test('render flipped', () => {
  const { container } = render(
    <AppContext.Provider value={Units['c'].convert}>
      <MarkerContent {...DEFAULT_CONFIG} flip />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});

test('render - fahrenheit', () => {
  const { container } = render(
    <AppContext.Provider value={Units['f'].convert}>
      <MarkerContent {...DEFAULT_CONFIG} flip={false} />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});

test('render flipped - fahrenheit', () => {
  const { container } = render(
    <AppContext.Provider value={Units['f'].convert}>
      <MarkerContent {...DEFAULT_CONFIG} flip />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <AppContext.Provider value={Units['c'].convert}>
      <MarkerContent data-test="id-01" {...DEFAULT_CONFIG} flip />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});
