import styled, { css } from 'styled-components';
import Color from '../../tokens/color';
import BorderWidth from '../../tokens/border-width';
import Typography from '../../tokens/typography';
import BorderRadius from '../../tokens/border-radius';
import Spacing from '../../tokens/spacing';

const styledElements = {
  MarkerContent: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `,
  MarkerWrapper: styled.dl`
    padding: 0 ${Spacing[6]};
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    overflow: hidden;
    outline: none;
    border: solid transparent ${BorderWidth[2]};
    border-radius: ${BorderRadius[8]};
    box-shadow: 0px 18px 15px 5px rgba(0,0,0,0.24);
  `,
  ArrowDown: styled.span`
    position: relative;
    z-index: 101;
    width: 0; 
    height: 0; 
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-top: 10px solid ${(props) => props.$backgroundColor};
  `,
  ArrowTop: styled.span`
    position: relative;
    z-index: 101;
    width: 0; 
    height: 0; 
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-bottom: 10px solid ${(props) => props.$backgroundColor};
  `,
  MarkerCity: styled.dt`
    width: 100%;
    ${css`
      ${Typography.city}
    `};
    color: ${Color.brightContent};
  `,
  MarkerTemperature: styled.dd`
    width: 100%;
    ${css`
      ${Typography.temperature}
    `};
    color: ${Color.brightContent};
  `,
};

export default styledElements;
