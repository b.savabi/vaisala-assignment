import React from 'react';
import Styled from './styled';

interface Props {
  /** The source of the img. */
  src: string;
  /** The alt attribute of the img. */
  alt?: string;
  /** Styles applied to the root element. */
  style?: Record<string, unknown>;
}

/**
 * `Icon` can be used to render images.
 * All props will be passed to the root element.
 */
const Icon: React.FC<Props> = (props) => <Styled.Icon {...props} />;

export default Icon;
