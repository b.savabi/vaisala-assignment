import React from 'react';
import { render } from '@testing-library/react';
import AvailableIcon from '../../icons/available.svg';

import Icon from '.';

test('render a SVG', () => {
  const { container } = render(<Icon src={AvailableIcon} alt="Available" />);
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(<Icon src={AvailableIcon} alt="Available" />);
  expect(container).toMatchSnapshot();
});
