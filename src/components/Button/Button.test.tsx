import React from 'react';
import { render } from '@testing-library/react';

import Button from '.';

test('render content', () => {
  const { container } = render(
    <Button>
      <div className="content">content</div>
    </Button>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <Button data-test="id-01">
      <div className="content">content</div>
    </Button>
  );
  expect(container).toMatchSnapshot();
});
