import styled, { css } from 'styled-components';
import Color from '../../tokens/color';
import Dimension from '../../tokens/dimension';
import BorderWidth from '../../tokens/border-width';
import Typography from '../../tokens/typography';

const styledElements = {
  Button: styled.button`
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    overflow: hidden;
    background: ${Color.interactiveContainer};
    min-height: ${Dimension[48]};
    min-width: ${Dimension[48]};
    outline: none;
    border: solid transparent ${BorderWidth[2]};
    color: ${Color.brightContent};
    ${css`
      ${Typography.interactive}
    `};
    &:focus {
      outline: none;
      border-color: ${Color.focusedBorder};
    }
  `,
};

export default styledElements;
