import React from 'react';
import { render } from '@testing-library/react';

import AppLayout from '.';

test('render content', () => {
  const { container } = render(
    <AppLayout>
      <div className="content">content</div>
    </AppLayout>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <AppLayout data-test="id-01">
      <div className="content">content</div>
    </AppLayout>
  );
  expect(container).toMatchSnapshot();
});
