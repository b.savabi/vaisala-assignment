import styled from 'styled-components';
import Color from '../../tokens/color';

const styledElements = {
  AppLayout: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    overflow: hidden;
    height: 100vh;
    width: 100vw;
    background: ${Color.lightContainer};
  `,
};

export default styledElements;
