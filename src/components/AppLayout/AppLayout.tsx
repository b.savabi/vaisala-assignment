import React from 'react';
import Styled from './styled';

interface Props {
  /**
   * Callback fired when user drop a file on the area.
   *
   * @param {object} event The event source of the callback.
   */
  onDrop?: React.MouseEventHandler<HTMLDivElement>;
  /**
   * Callback fired when user drop a file on the area.
   *
   * @param {object} event The event source of the callback.
   */
  onDragOver?: React.MouseEventHandler<HTMLDivElement>;
  /**
   * The content of the app. Consider using `AppBanner` and `AppContent`.
   */
  children: React.ReactNode;
}

/**
 * `AppLayout` can be used to wrap the `AppBanner` and `AppContent` of the app.
 * All props will be passed to the root element.
 */
const AppLayout: React.FC<Props> = ({ children, ...otherProps }) => (
  <Styled.AppLayout {...otherProps}>{children}</Styled.AppLayout>
);

export default AppLayout;
