import styled, { css } from 'styled-components';
import BorderRadius from '../../tokens/border-radius';
import Color from '../../tokens/color';
import Spacing from '../../tokens/spacing';
import Typography from '../../tokens/typography';

const styledElements = {
  EmptyState: styled.div`
    position: absolute;
    top: 90px;
    left: 0;
    z-index: 99;
    display: flex;
    margin: ${Spacing[12]};
    padding: ${Spacing[12]};
    flex-direction: column;
    align-items: center;
    justify-content: center;
    overflow: hidden;
    height: 100vh;
    width: calc(100% - ${Spacing[24]});
    height: calc(100% - 120px);
    background: ${Color.emptyState};
    color: ${Color.brightContent};
    border-radius: ${BorderRadius[8]};
    text-align: center;
    ${css`
      ${Typography.description}
    `};
  `,
  EmptyStateTitle: styled.div`
    text-align: center;
    ${css`
      ${Typography.h1}
    `};
  `,
};

export default styledElements;
