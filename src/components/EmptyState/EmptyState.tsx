import React from 'react';
import Styled from './styled';
import Icon from '../Icon';

import EmptyStateIcon from '../../assets/empty-state.png';

interface Props {
  /**
   * The content of the EmptyState.
   */
  children: string;
}

/**
 * `EmptyState` can be used to wrap the `AppBanner` and `AppContent` of the app.
 * All props will be passed to the root element.
 */
const EmptyState: React.FC<Props> = ({ children, ...otherProps }) => (
  <Styled.EmptyState {...otherProps}>
    <Icon alt="Empty state" src={EmptyStateIcon} style={{ width: 100, height: 100 }} />
    <Styled.EmptyStateTitle>No data!</Styled.EmptyStateTitle>
    <span>{children}</span>
  </Styled.EmptyState>
);

export default EmptyState;
