import React from 'react';
import { render } from '@testing-library/react';

import EmptyState from '.';

test('render content', () => {
  const { container } = render(
    <EmptyState>
      Message
    </EmptyState>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <EmptyState data-test="id-01">
      Message
    </EmptyState>
  );
  expect(container).toMatchSnapshot();
});
