import styled from 'styled-components';
import Color from '../../tokens/color';
import Spacing from '../../tokens/spacing';

const styledElements = {
  AppBanner: styled.div`
    position: relative;
    z-index: 100;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    background: ${Color.primaryColor};
    width: 100vw;
    min-height: 90px;
    padding: ${Spacing[16]};
  `,
};

export default styledElements;
