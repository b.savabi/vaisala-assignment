import React from 'react';
import { render } from '@testing-library/react';

import AppBanner from '.';

test('render content', () => {
  const { container } = render(
    <AppBanner>
      content
    </AppBanner>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <AppBanner data-test="id-01">
      content
    </AppBanner>
  );
  expect(container).toMatchSnapshot();
});
