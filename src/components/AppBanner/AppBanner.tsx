import React from 'react';
import Styled from './styled';

interface Props {
  /**
   * The content of the `AppBanner`, consider using `Icon`.
   */
  children: React.ReactNode;
}

/**
 * `AppBanner` component, can be used to render the banner of the app.
 * All props will be passed to the root element.
 */
const AppBanner: React.FC<Props> = ({ children, ...otherProps }) => (
  <Styled.AppBanner {...otherProps}>{children}</Styled.AppBanner>
);

export default AppBanner;
