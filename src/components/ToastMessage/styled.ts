import styled, { css } from 'styled-components';
import Color from '../../tokens/color';
import Spacing from '../../tokens/spacing';
import Typography from '../../tokens/typography';

const styledElements = {
  ToastMessage: styled.div`
    z-index: 99;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    overflow: hidden;
    padding: ${Spacing[12]};
    background: ${Color.primaryColor};
    color: ${Color.brightContent};
    width: 100vw;
    ${css`
      ${Typography.description}
    `};
  `,
};

export default styledElements;
