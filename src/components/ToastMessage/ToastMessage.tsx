import React from 'react';
import Styled from './styled';

interface Props {
  /**
   * The content of the ToastMessage.
   */
  children: string;
}

/**
 * `ToastMessage` can be used to wrap the `AppBanner` and `AppContent` of the app.
 * All props will be passed to the root element.
 */
const ToastMessage: React.FC<Props> = ({ children, ...otherProps }) => (
  <Styled.ToastMessage {...otherProps}><span>{children}</span></Styled.ToastMessage>
);

export default ToastMessage;
