import React from 'react';
import { render } from '@testing-library/react';

import ToastMessage from '.';

test('render content', () => {
  const { container } = render(
    <ToastMessage>
      <div className="content">content</div>
    </ToastMessage>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <ToastMessage data-test="id-01">
      <div className="content">content</div>
    </ToastMessage>
  );
  expect(container).toMatchSnapshot();
});
