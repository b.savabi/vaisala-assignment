import React, { useMemo } from 'react';
import ReactMapGL, { Marker } from 'react-map-gl';
import getKey from '../../utils/get-key';
import MarkerContent, { height, width } from '../MarkerContent';

interface Props {
  cities: undefined | {
    city: string,
    lat: string,
    lon: string,
    temp: string
  }[]
}

const INITIAL_CONFIG = {
  latitude: 60.192059,
  longitude: 24.945831,
  zoom: 7,
};

export const Map: React.FC<Props> = ({ cities }) => {
  const [viewport, setViewport] = React.useState(INITIAL_CONFIG);
  const markers = useMemo(() => !!cities && cities.map((city) => {
    const latitude = parseFloat(city.lat);
    const longitude = parseFloat(city.lon);
    const flip = latitude > 45;
    return (
      <Marker
        offsetTop={flip ? 10 : -height}
        offsetLeft={-1 * (width / 2)}
        key={city.city}
        latitude={latitude}
        longitude={longitude}
      >
        <MarkerContent
          {...getKey('marker-content')}
          flip={flip}
          city={city.city}
          temperature={parseFloat(city.temp)}
        />
      </Marker>
    );
  }), [cities]);

  return (
    <ReactMapGL
      {...viewport}
      width="100%"
      height="100%"
      mapStyle="mapbox://styles/bsavabi/cknyfmu4y1ohy17npfgc5psjo"
      onViewportChange={(v) => { setViewport(v); }}
    >
      {markers}
    </ReactMapGL>
  );
};

export default Map;
