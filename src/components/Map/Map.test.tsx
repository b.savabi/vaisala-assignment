import React from 'react';
import { render } from '@testing-library/react';
import AppContext from '../../App/AppContext';
import Units from '../../utils/units';

import Map from '.';

const CITIES = [
  {
    city: "Helsinki",
    lat: "60.1676",
    lon: "24.9421",
    temp: "0"
  },{
    city: "Moscow",
    lat: "55.7558",
    lon: "37.6173",
    temp: "-15.0"
  },{
    city: "Berlin",
    lat: "52.5200",
    lon: "13.4050",
    temp: "25.0"
  }
];


const DEFAULT_CONFIG = {
  cities: CITIES
};

test('render', () => {
  const { container } = render(
    <AppContext.Provider value={Units['c'].convert}>
      <Map {...DEFAULT_CONFIG} />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});

test('render flipped', () => {
  const { container } = render(
    <AppContext.Provider value={Units['c'].convert}>
      <Map {...DEFAULT_CONFIG} />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});

test('render - fahrenheit', () => {
  const { container } = render(
    <AppContext.Provider value={Units['f'].convert}>
      <Map {...DEFAULT_CONFIG} flip={false} />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});

test('render flipped - fahrenheit', () => {
  const { container } = render(
    <AppContext.Provider value={Units['f'].convert}>
      <Map {...DEFAULT_CONFIG} />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <AppContext.Provider value={Units['c'].convert}>
      <Map {...DEFAULT_CONFIG} />
    </AppContext.Provider>
  );
  expect(container).toMatchSnapshot();
});
